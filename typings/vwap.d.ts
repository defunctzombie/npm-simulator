declare module 'vwap' {
    export default function vwap(records: [number, number][]): number;
}
