/* eslint-disable */
const path = require('path');

module.exports = {
    assetPrefix: process.env.NODE_ENV === 'production' ? '/npm-simulator' : '',
    // gitlab pages should keep the trailing slash for use-query-params to function
    trailingSlash: false,
    webpack(config) {
        config.resolve.alias['~'] = path.join(__dirname);
        return config;
    },
};
