import { useMemo } from 'react';
import { Paper, Container, Grid } from '@material-ui/core';
import { useQueryParam } from 'use-query-params';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, ResponsiveContainer } from 'recharts';
import vwap from 'vwap';

import BookEntry from '~/common/BookEntry';
import BookSideTable from '~/components/BookSideTable';
import BookEntryArrayQuerySerialization from '~/common/BookEntryArrayQuerySerialization';

export default function Home(): JSX.Element {
    const [bids, setBids] = useQueryParam<BookEntry[]>('bids', BookEntryArrayQuerySerialization);
    const [asks, setAsks] = useQueryParam<BookEntry[]>('asks', BookEntryArrayQuerySerialization);

    const sortedBids = useMemo(() => {
        // sort bids highest -> lowest
        return [...bids].sort((a, b) => {
            return b.price - a.price;
        });
    }, [bids]);

    const sortedAsks = useMemo(() => {
        // sort asks lowest -> highest
        return [...asks].sort((a, b) => {
            return a.price - b.price;
        });
    }, [asks]);

    // flatten data for our chat
    const chartData = useMemo(() => {
        const prices = new Map<number, { price: number; bid: number; ask: number }>();

        for (const bid of bids) {
            const existing = prices.get(bid.price);
            if (existing) {
                existing.bid += bid.size;
                prices.set(bid.price, existing);
                continue;
            }
            prices.set(bid.price, { price: bid.price, bid: bid.size, ask: 0 });
        }

        for (const ask of asks) {
            const existing = prices.get(ask.price);
            if (existing) {
                existing.ask += ask.size;
                prices.set(ask.price, existing);
                continue;
            }
            prices.set(ask.price, { price: ask.price, bid: 0, ask: ask.size });
        }

        const chartData = [...prices.values()];
        chartData.sort((a, b) => a.price - b.price);

        if (chartData.length === 0) {
            return chartData;
        }

        const bucketSize = 25;
        const buckets: { price: number; ask: number; bid: number }[] = [];
        let nextPrice = chartData[0].price + bucketSize;

        let bidRecords: [number, number][] = [];
        let askRecords: [number, number][] = [];

        for (const item of chartData) {
            if (item.price > nextPrice) {
                if (askRecords.length > 0) {
                    const askVwap = vwap(askRecords);
                    const askQty = askRecords.reduce((prev, record) => {
                        return prev + record[0];
                    }, 0);
                    buckets.push({ price: askVwap, bid: 0, ask: askQty });
                }

                if (bidRecords.length > 0) {
                    const bidVwap = vwap(bidRecords);
                    const bidQty = bidRecords.reduce((prev, record) => {
                        return prev + record[0];
                    }, 0);
                    buckets.push({ price: bidVwap, bid: bidQty, ask: 0 });
                }

                bidRecords = [];
                askRecords = [];
                nextPrice = item.price + bucketSize;
            }

            if (item.bid > 0) {
                bidRecords.push([item.bid, item.price]);
            }

            if (item.ask > 0) {
                askRecords.push([item.ask, item.price]);
            }
        }
        if (askRecords.length > 0) {
            const askVwap = vwap(askRecords);
            const askQty = askRecords.reduce((prev, record) => {
                return prev + record[0];
            }, 0);
            buckets.push({ price: askVwap, bid: 0, ask: askQty });
        }

        if (bidRecords.length > 0) {
            const bidVwap = vwap(bidRecords);
            const bidQty = bidRecords.reduce((prev, record) => {
                return prev + record[0];
            }, 0);
            buckets.push({ price: bidVwap, bid: bidQty, ask: 0 });
        }

        return buckets;
    }, [bids, asks]);

    const clearingInfo = useMemo(() => {
        let clearingPrice = 0;
        let tradeVolume: number = 0;

        if (sortedBids.length === 0 || sortedBids.length === 0) {
            return { price: clearingPrice, volume: tradeVolume };
        }

        const bidsCopy = sortedBids.map((x) => Object.assign({}, x));
        const asksCopy = sortedAsks.map((x) => Object.assign({}, x));

        // grab the first bid and ask
        let candidateBid = bidsCopy.shift();
        let candidateAsk = asksCopy.shift();

        while (candidateBid && candidateAsk) {
            // no crossing, nothing to match
            if (candidateAsk.price > candidateBid.price) {
                return { price: clearingPrice, volume: tradeVolume };
            }

            const tradeQty = Math.min(candidateBid.size, candidateAsk.size);
            tradeVolume += tradeQty;
            clearingPrice = candidateBid.price;

            candidateBid.size -= tradeQty;
            candidateAsk.size -= tradeQty;

            console.log('trade', tradeVolume, '@', clearingPrice);

            // move to next bid
            if (candidateBid.size <= 0) {
                candidateBid = bidsCopy.shift();
            }

            // move to next ask
            if (candidateAsk.size <= 0) {
                candidateAsk = asksCopy.shift();
            }
        }

        return { price: clearingPrice, volume: tradeVolume };
    }, [sortedBids, sortedAsks]);

    return (
        <Container>
            <Grid container spacing={1}>
                <Grid item xs={12} style={{ paddingTop: '16px' }}>
                    <Paper>
                        <ResponsiveContainer width="100%" height={400}>
                            <BarChart data={chartData} barSize={10}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis
                                    dataKey="price"
                                    allowDecimals={false}
                                    type="number"
                                    interval="preserveEnd"
                                    domain={['dataMin - 10', 'dataMax + 10']}
                                    tickCount={40}
                                    tickFormatter={(val) => Number(val).toFixed(2)}
                                />
                                <YAxis />
                                <Bar dataKey="bid" fill="green" />
                                <Bar dataKey="ask" fill="red" />
                            </BarChart>
                        </ResponsiveContainer>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper style={{ padding: '8px' }}>Clearing Price: {clearingInfo.price}</Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper style={{ padding: '8px' }}>Volume: {clearingInfo.volume}</Paper>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <Grid item md={6} xs={12}>
                            <Paper>
                                <BookSideTable
                                    title="bids"
                                    data={sortedBids}
                                    fillQty={clearingInfo.volume}
                                    onChangeData={(newData) => setBids(newData)}
                                />
                            </Paper>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Paper>
                                <BookSideTable
                                    title="asks"
                                    data={sortedAsks}
                                    fillQty={clearingInfo.volume}
                                    onChangeData={(newData) => setAsks(newData)}
                                />
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    );
}
