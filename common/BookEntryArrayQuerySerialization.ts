import BookEntry from './BookEntry';

const BookEntryArrayQuerySerialization = {
    encode(book: BookEntry[]) {
        return Buffer.from(JSON.stringify(book)).toString('base64');
    },

    decode(strValue: string | null | undefined | (string | null)[]): BookEntry[] {
        if (!strValue) {
            return [];
        }

        if (Array.isArray(strValue)) {
            return [];
        }

        return JSON.parse(Buffer.from(strValue, 'base64').toString());
    },
};

export default BookEntryArrayQuerySerialization;
