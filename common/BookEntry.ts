interface BookEntry {
    price: number;
    size: number;
}

export default BookEntry;
