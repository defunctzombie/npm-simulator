import { FormEvent, useState, useCallback } from 'react';
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button,
    Typography,
    IconButton,
    TextField,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';

import BookEntry from '~/common/BookEntry';

interface BookSideTableProps {
    data: BookEntry[];
    title: string;
    fillQty: number;
    onChangeData: (newData: BookEntry[]) => void;
}

export default function BookSideTable(props: BookSideTableProps) {
    const data = props.data;

    const [addPrice, setAddPrice] = useState(1);
    const [addQty, setAddQty] = useState(1);

    const [addingRow, setAddingRow] = useState<boolean>(false);

    const handleOnSubmit = useCallback(
        (ev: FormEvent<HTMLFormElement>) => {
            if (!addingRow) {
                return;
            }
            data.push({ price: addPrice, size: addQty });
            props.onChangeData(data);

            setAddQty(1);
            setAddPrice(1);
            setAddingRow(false);
            ev.preventDefault();
        },
        [addPrice, addQty, addingRow, data, props],
    );

    let fillQtyRemaining = props.fillQty;

    return (
        <>
            <Typography variant="h5" style={{ padding: '8px' }}>
                {props.title}
            </Typography>

            <form onSubmit={handleOnSubmit}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell align="right" width="50%">
                                Price
                            </TableCell>
                            <TableCell align="right" width="50%">
                                Qty
                            </TableCell>
                            <TableCell align="right" width="50%">
                                Fill Qty
                            </TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((entry) => {
                            const fill = Math.min(entry.size, fillQtyRemaining);
                            fillQtyRemaining = fillQtyRemaining - fill;

                            return (
                                <TableRow key={entry.price}>
                                    <TableCell align="right">{entry.price}</TableCell>
                                    <TableCell align="right">
                                        <TextField
                                            name="qty"
                                            inputProps={{
                                                type: 'number',
                                                style: { textAlign: 'right' },
                                            }}
                                            fullWidth={true}
                                            value={entry.size}
                                            onChange={(ev) => {
                                                const newVal = +ev.target.value;
                                                const oldVal = entry.size;

                                                for (const item of data) {
                                                    if (item.price === entry.price) {
                                                        item.size = item.size - oldVal + newVal;
                                                    }
                                                }

                                                props.onChangeData(data);
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell align="right">{fill}</TableCell>
                                    <TableCell>
                                        <IconButton
                                            size="small"
                                            onClick={() => {
                                                const idx = data.findIndex((item) => {
                                                    return item.price === entry.price;
                                                });

                                                if (idx < 0) {
                                                    return data;
                                                }

                                                data.splice(idx, 1);
                                                props.onChangeData(data);
                                            }}
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                        {addingRow && (
                            <TableRow>
                                <TableCell align="right">
                                    <TextField
                                        name="price"
                                        inputProps={{
                                            type: 'number',
                                            style: { textAlign: 'right' },
                                        }}
                                        fullWidth={true}
                                        value={addPrice}
                                        onChange={(ev) => setAddPrice(+ev.target.value)}
                                    />
                                </TableCell>
                                <TableCell align="right">
                                    <TextField
                                        name="qty"
                                        inputProps={{
                                            type: 'number',
                                            style: { textAlign: 'right' },
                                        }}
                                        fullWidth={true}
                                        value={addQty}
                                        onChange={(ev) => setAddQty(+ev.target.value)}
                                    />
                                </TableCell>
                                <TableCell></TableCell>
                                <TableCell size="small" style={{ whiteSpace: 'nowrap' }}>
                                    <IconButton type="submit">
                                        <DoneIcon />
                                    </IconButton>
                                    <IconButton
                                        onClick={() => {
                                            setAddQty(1);
                                            setAddPrice(1);
                                            setAddingRow(false);
                                        }}
                                    >
                                        <CloseIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </form>
            <Button onClick={() => setAddingRow(true)}>Add</Button>
        </>
    );
}
